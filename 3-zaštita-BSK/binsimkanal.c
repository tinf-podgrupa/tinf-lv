//
// Created by marin on 16.01.16..
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

FILE *open_file(char* filename, char* mode);

void invert_bits(FILE *output, FILE *input, float *percentage);

bool should_invert_bit(float *percentage);

void invert_byte_bits(unsigned char *buffer, float *percentage);

int main(int argc, char *argv[]) {
    if(argc != 4) {
        printf("Invalid number of arguments suplied!\n");
        exit(1);
    }
    FILE *input = open_file(argv[1], "rb");
    if (!input) {
        printf("Unable to open input file(%s)!\n", argv[1]);
    }
    float percentage;
    sscanf(argv[2], "%f", &percentage);
    FILE *output = open_file(argv[3], "wb");
    if (!output) {
        printf("Unable to open output file(%s)!\n", argv[3]);
    }
    srand((unsigned int)time(NULL));
    invert_bits(input, output, &percentage);
    printf("Bits inverted! CLosing files...");
    fclose(input);
    fclose(output);
}

void invert_bits(FILE *input, FILE *output, float *percentage) {
    long filelen;
    fseek(input, 0, SEEK_END);
    filelen = ftell(input);
    rewind(input);
    unsigned char* buffer = (unsigned char *)malloc((filelen+1)*sizeof(unsigned char));
    fread(buffer, sizeof(unsigned char), filelen, input);
    int i = 0;
    for(i; i < filelen; i++) {
        invert_byte_bits(buffer, percentage);
        fwrite(buffer, sizeof(unsigned char), 1, output);
        buffer++;
    }
}

void invert_byte_bits(unsigned char *buffer, float *percentage) {
    unsigned char bits[8];
    unsigned char mask = 1;
    int i = 0;
    for(i; i < 8; i++) {
        if((*buffer & (mask << i)) != 0) {
            if(should_invert_bit(percentage)) {
                bits[(7-i)] = '0';
            } else {
                bits[(7-i)] = '1';
            }
        } else {
            if(should_invert_bit(percentage)) {
                bits[(7-i)] = '1';
            } else {
                bits[(7-i)] = '0';
            }
        }
    }
    *buffer = 0;
    for ( i = 0; i < 8; ++i ) {
        *buffer |= (bits[i] == '1') << (7 - i);
    }
}

FILE *open_file(char* filename, char* mode) {
    printf("Opening file %s...\n", filename);
    return fopen(filename, mode);
}

bool should_invert_bit(float *percentage) {
    int random_num = rand() % 99 + 1;
    if(random_num <= ((int)(*percentage * 100))) return true;
    return false;
}