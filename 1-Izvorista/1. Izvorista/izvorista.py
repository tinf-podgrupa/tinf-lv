# -*- coding: utf-8 -*-

import matplotlib.pylab as plt
import numpy as np
#import scipy.stats as stats


def u_niz(broj, duljina=0):
    a = "xy"
    if duljina > 0:
        niz = u_niz(broj, 0)
        for i in range(duljina - len(niz)):
            niz = a[0] + niz
        return niz
    else:
        return ((broj == 0) and a[0]) or (u_niz(broj // 2, 0).lstrip(a[0]) + a[broj % 2])


def u_broj(niz):
    broj = 0
    for s in niz:
        broj *= 2
        if s == 'y':
            broj += 1
    return broj


def entropija(razd):
    probs = razd[np.nonzero(razd)]
    return - np.sum(probs * np.log2(probs))


def prebroji_simbole(niz, duljina):
    n = 2 ** duljina
    simboli = [u_niz(i, duljina) for i in range(n)]
    polozaji = [i + 0.5 for i in range(n)]
    razd = np.zeros(n, dtype=float)

    for i in range(0, len(niz), duljina):
        np.add.at(razd, u_broj(niz[i:i + duljina]), 1)
    razd /= len(niz) / duljina

    print("  S = {}".format(np.sum(razd)))
    print("  H = {}".format(entropija(razd)))

    plt.figure(figsize=(n / 4, 6))
    plt.bar(polozaji, razd, align='center', width=1.0)
    plt.xticks(polozaji, simboli, rotation='vertical')
    plt.yticks(np.arange(0, 1.01, 0.1))
    plt.xlabel("simbol")
    plt.xlim([0, n])
    plt.ylim([0, 1.0])
    #plt.show()
    return plt


def main():
    datoteke = ["i{}.txt".format(i) for i in [1, 2, 3, 4]]
    for d in datoteke:
        niz = open(d).read()
        for n in [1, 2, 4, 8]:
            ime = d[0:2] + "s{}.png".format(n)
            print(ime)
            prebroji_simbole(niz, n)
            plt.savefig(ime, bbox_inches='tight')


# prebroji_simbole("xyxyxxyxyyyxyyyxyxxxyxyxyyyxxyyxyxyxyxyyxxy", 4)
main()
