#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <string.h>	// stringbuilder.c, dictionary.c
#include <malloc.h>	// stringbuilder.c, dictionary.c

#define DICT_CAPACITY (int)(65536)
#define MAX_STR DICT_CAPACITY + 1


// string.c ------------------------------------------------------------------------------------------------

typedef struct {
	char *chars;
	int len;
} String;

int str_equals(String *s1, String *s2) {
	if (s1 == s2) return 1;
	if (s1->len != s2->len) return 0;
	for (int i = 0; i < s1->len; i++)
		if (s1->chars[i] != s2->chars[i]) return 0;
	return 1;
}
// \string.c -----------------------------------------------------------------------------------------------


// dictionary.c --------------------------------------------------------------------------------------------

typedef struct {
	String *key;
	int value;
} KeyValuePair;

unsigned long _get_hash(String *niz) {
	unsigned long h = 65599;
	for (int i = 0; i < niz->len; i++) h = 2053 * h + niz->chars[i]; // 997 0.074, 2053 0.072699
	return h;
}

typedef struct {
	KeyValuePair *data;
	int capacity;
	int count;
	int collisions;
} Dictionary;

void _dict_init(Dictionary *dict, int capacity) {
	dict->data = calloc(capacity, sizeof(KeyValuePair));
	dict->capacity = capacity;
	dict->count = 0;
	dict->collisions = 0;
}

Dictionary* new_Dictionary(int min_capacity) {
	Dictionary *d = malloc(sizeof(Dictionary));
	_dict_init(d, min_capacity*1.3);
	return d;
}

int dict_add_or_get_value(Dictionary *dict, String *key, int value) {  // vra�a KeyValuePair.value ako postoji, ina�e -1
	int addr = _get_hash(key) % dict->capacity;
	int a = addr;
	while (dict->data[a].key != NULL) {
		if (str_equals(key, dict->data[a].key)) return dict->data[a].value;
		if (++a == dict->capacity) a = 0;
	}
	dict->data[a] = (KeyValuePair) { key, value };
	dict->count++;
	dict->collisions += (a >= addr) ? a - addr : a - addr + dict->capacity;
	return -1;
}

int dict_get_value(Dictionary *dict, String *key) {  // vra�a KeyValuePair.value ako postoji, ina�e -1
	int a = _get_hash(key) % dict->capacity;
	while (dict->data[a].key != NULL) {
		if (str_equals(key, dict->data[a].key)) return dict->data[a].value;
		if (++a == dict->capacity) a = 0;
	}
	return -1;
}

void _dict_debug_print_distribution(Dictionary *dict) {
	for (int i = 0; i < dict->capacity; i++) {
		if (dict->data[i].key == NULL) putc(' ', stdout);
		else putc(220, stdout);
	}
	printf("\ncollisions: %f", ((float)dict->collisions*dict->count) / dict->capacity / dict->capacity);
}
// \dictionary.c --------------------------------------------------------------------------------------------


// stringbuilder.c ------------------------------------------------------------------------------------------

typedef struct {
	char* chars;
	int len;
} StringBuilder;

StringBuilder* new_StringBuilder(int capacity) {
	StringBuilder* sb = malloc(sizeof(StringBuilder));
	sb->chars = malloc(capacity*sizeof(char));
	sb->chars[0] = '\0';
	sb->len = 0;
	return sb;
}

void sb_appendc(StringBuilder* sb, char c) {
	sb->chars[sb->len++] = c;
	sb->chars[sb->len] = '\0';
}

void sb_appendstr(StringBuilder* sb, String *s) {
	for (int i = 0; i < s->len; i++) sb->chars[i] = s->chars[i];
	sb->len = s->len;
}

void sb_clear(StringBuilder* sb) {
	sb->chars[0] = '\0';
	sb->len = 0;
}

String *sb_to_string(StringBuilder* sb) {
	char *s = malloc(sb->len*sizeof(char));
	for (int i = 0; i <= sb->len; i++) s[i] = sb->chars[i];
	String *str = malloc(sizeof(String));
	*str = (String) { s, sb->len };
	return str;
}
// \stringbuilder.c ------------------------------------------------------------------------------------------


// ---- LZW koder ----

int main(int argc, char **argv) {
	Dictionary *dict = new_Dictionary((int)(DICT_CAPACITY - 256));
	int n = 256;

	FILE *fin = fopen(argv[1], "rb");
	FILE* fout = fopen(argv[2], "wb");

	StringBuilder* w = new_StringBuilder(MAX_STR);
	char c = fgetc(fin);
	sb_appendc(w, c);
	unsigned short code_prev = c;
	while (c = fgetc(fin), !feof(fin)) {
		sb_appendc(w, c);
		int code;  // mora biti int zbog vrijednosti -1
		if (n < DICT_CAPACITY)
			code = dict_add_or_get_value(dict, sb_to_string(w), n);  // pronala�enje poznate rije�i ili dodavanje (-1)
		else
			code = dict_get_value(dict, sb_to_string(w));  // pronala�enje poznate rije�i ili nepronala�enje (-1)

		if (code == -1) {  // nepoznato (do maloprije ako rje�nik nije pun)
			fwrite(&code_prev, sizeof(unsigned short), 1, fout);  // stavlja kod zadnje poznate rije�i na izlaz
			sb_clear(w);
			sb_appendc(w, c);  // zapo�inje novu rije�
			code_prev = c;
			if (n < DICT_CAPACITY)
				n++;
		}
		else  // poznato
			code_prev = code;
	}
	fwrite(&code_prev, sizeof(unsigned short), 1, fout);
	fcloseall();

	return 0;
}
