//
// Created by MarinPetrunic on 30.12.2015..
//
#include<stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

//structures
struct ForrestTreeNode {
    unsigned char byte;
    long appearance_number;
    float frequency;
    char code[256];
    struct ForrestTreeNode * previous;
    struct ForrestTreeNode * next;
    struct ForrestTreeNode* left;
    struct ForrestTreeNode* right;
};

//function declaration
struct ForrestTreeNode* generate_byte_codes_table(FILE *table_file, char *bytes, bool close_file);
char* read_file_bytes(FILE* file, bool close_file);
FILE *open_file(char* filename, char* mode);
struct ForrestTreeNode * generate_empty_bytes_list();
void increment_byte_appearance_number(struct ForrestTreeNode *bytes_list, unsigned char byte);
struct ForrestTreeNode *create_forrest_tree();
void sort_list(struct ForrestTreeNode **list);
void calculate_frequency(struct ForrestTreeNode *list, long total);
struct ForrestTreeNode* generate_tree(struct ForrestTreeNode *list);
struct ForrestTreeNode *create_root_tree_node(struct ForrestTreeNode *right, struct ForrestTreeNode *left);
void fill_codes(struct ForrestTreeNode *pNode, struct ForrestTreeNode *pTreeNode);

void add_code_to_list(struct ForrestTreeNode *pNode, unsigned char byte, char code[256]);

void write_code_to_file(FILE *pIobuf, struct ForrestTreeNode *pNode);

void encode_file(FILE *pIobuf, struct ForrestTreeNode *pNode, char *bytes);

char *get_code(struct ForrestTreeNode *pNode, unsigned char *byte);

void write(FILE *pIobuf, char buffer[8], int* pos);

void swap(struct ForrestTreeNode *first, struct ForrestTreeNode *second);

void print_list(struct ForrestTreeNode *pNode);

//Main function
int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf("Wrong number of arguments supplied!\n");
        exit(1);
    }
    FILE *input = open_file(argv[1], "rb");
    if (!input) {
        printf("Unable to open input file(%s)!\n", argv[1]);
    }
    fseek(input, 0L, SEEK_END);
    printf("Size of input file is %ld kB\n", ftell(input));
    fseek(input, 0L, SEEK_SET);
    char *input_bytes = read_file_bytes(input, true);
    FILE *byte_code_table_file = open_file(argv[2], "w");
    if (!byte_code_table_file) {
        printf("Unable to open/create file(%s)!\n", argv[2]);
    }
    struct ForrestTreeNode* codeNode = generate_byte_codes_table(byte_code_table_file, input_bytes, true);
    FILE *output_file = open_file(argv[3], "wb");
    if(!output_file) {
        printf("Unable to open/create file(%s)!\n", argv[3]);
    }
    encode_file(output_file, codeNode, input_bytes);
    fseek(output_file, 0L, SEEK_END);
    printf("Size of output file is %ld B\n", ftell(output_file));
    fclose(output_file);
    printf("Encoding finished!");
}

void encode_file(FILE *pIobuf, struct ForrestTreeNode *pNode, char *bytes) {
    unsigned char *byte;
    char buffer[8];
    int buf_pos = 0;
    for(byte = (unsigned char *) bytes; *byte != '\0'; byte++){
        char* code = get_code(pNode, byte);
        if(code == NULL) {
            continue;
        }
        char* bit;
        for(bit = code; *bit != '\0'; bit++) {
            buffer[buf_pos++] = *bit;
            write(pIobuf, buffer, &buf_pos);
        }
    }
    if(buf_pos < 8 && buf_pos != 0) {
        for(buf_pos; buf_pos <= 8; buf_pos++) {
            buffer[buf_pos] = '1';
        }
        write(pIobuf, buffer, &buf_pos);
    }
}

void write(FILE *pIobuf, char buffer[8], int* pos) {
    if(*pos >= 8) {
        int i;
        unsigned char result = 0;
        for(i = 0; i < 8; ++i) {
            result |= (buffer[i] == '1') << (7 -i );
        }
        fwrite(&result, sizeof(result), 1, pIobuf);
        *pos = 0;
    }
}

char *get_code(struct ForrestTreeNode *pNode, unsigned char *byte) {
    while(pNode != NULL) {
        if(pNode->byte == *byte) {
            return pNode->code;
        }
        pNode = pNode->next;
    }
    return NULL;
}

struct ForrestTreeNode* generate_byte_codes_table(FILE *table_file, char *bytes, bool close_file) {
    struct ForrestTreeNode * list = generate_empty_bytes_list();
    char* byte;
    long total_appearance = 0;
    for(byte = bytes; *byte != '\0'; byte++) {
        increment_byte_appearance_number(list, (unsigned char)*byte);
        total_appearance++;
    }
    printf("Calculating byte frequencies...\n");
    calculate_frequency(list, total_appearance);
    struct ForrestTreeNode* root = generate_tree(list);
    struct ForrestTreeNode * code_list = generate_empty_bytes_list();
    printf("Generating codes for each byte...\n");
    fill_codes(code_list, root);
    write_code_to_file(table_file, code_list);
    if(close_file) {
        fclose(table_file);
    }
    return code_list;
}

void write_code_to_file(FILE *pIobuf, struct ForrestTreeNode *pNode) {
    while(pNode != NULL) {
        fprintf(pIobuf, "%s\n", pNode->code);
        pNode = pNode->next;
    }
}

void fill_codes(struct ForrestTreeNode *pListNode, struct ForrestTreeNode *pTreeNode) {
    if(pTreeNode->left == NULL && pTreeNode->right == NULL) {
        add_code_to_list(pListNode, pTreeNode->byte, pTreeNode->code);
        return;
    }
    strcat(pTreeNode->left->code, pTreeNode->code);
    strcat(pTreeNode->right->code, pTreeNode->code);
    fill_codes(pListNode, pTreeNode->left);
    fill_codes(pListNode, pTreeNode->right);
}

void add_code_to_list(struct ForrestTreeNode *pNode, unsigned char byte, char code[256]) {
    do {
        if(pNode->byte == byte) {
            strcpy(pNode->code, code);
            return;
        }
        pNode = pNode->next;
    } while (pNode != NULL);
}

struct ForrestTreeNode* generate_tree(struct ForrestTreeNode *list) {
    sort_list(&list);
    if(list->next == NULL) {
        return list;
    }
    struct ForrestTreeNode* right = list;
    struct ForrestTreeNode* left = list->next;
    struct ForrestTreeNode* root = create_root_tree_node(right, left);
    list = list->next->next;
    right->next = NULL;
    left->previous = NULL;
    left->next = NULL;
    if(list != NULL) {
        list->previous = root;
    }
    root->next = list;
    return generate_tree(root);
}

void print_list(struct ForrestTreeNode *pNode) {
    while(pNode != NULL) {
        printf("Byte %X has freq %.02f \n", pNode->byte, pNode->frequency);
        pNode = pNode->next;
    }
    printf("\n");
}

struct ForrestTreeNode *create_root_tree_node(struct ForrestTreeNode *right, struct ForrestTreeNode *left) {
    struct ForrestTreeNode* root = create_forrest_tree();
    root->left = left;
    root->right = right;
    strcpy(root->left->code,"1");
    strcpy(root->right->code,"0");
    root->frequency = left->frequency + right->frequency;
    return root;
}

void calculate_frequency(struct ForrestTreeNode *list, long total) {
    while (list != NULL) {
        list->frequency = (float)list->appearance_number / total;
        list = list->next;
    }
}

//merge sort
void sort_list(struct ForrestTreeNode **list) {
    if(*list == NULL || (*list)->next == NULL) return;
    struct ForrestTreeNode * first = *list;
    while(first != NULL) {
        struct ForrestTreeNode * second = first;
        while(second != NULL) {
            if(second->next == NULL) break;
            if(second->frequency > second->next->frequency) {
                swap(second, second->next);
            }
            second = second->next;
        }
        first = first->next;

    }
}

void swap(struct ForrestTreeNode *left, struct ForrestTreeNode *right) {
    struct ForrestTreeNode *temp = create_forrest_tree();
    memcpy(temp->code, left->code, sizeof(left->code));
    temp->byte = left->byte;
    temp->appearance_number = left->appearance_number;
    temp->frequency = left->frequency;
    memcpy(left->code, right->code, sizeof(right->code));
    left->byte = right->byte;
    left->appearance_number = right->appearance_number;
    left->frequency = right->frequency;
    memcpy(right->code, temp->code, sizeof(temp->code));
    right->byte = temp->byte;
    right->appearance_number = temp->appearance_number;
    right->frequency = temp->frequency;
}

void increment_byte_appearance_number(struct ForrestTreeNode *bytes_list, unsigned char byte) {
    do {
        if(bytes_list->byte == byte) {
            bytes_list->appearance_number++;
            break;
        }
        bytes_list = bytes_list->next;
    } while (bytes_list != NULL);
}

char* read_file_bytes(FILE* file, bool close_file) {
    fseek(file, 0, SEEK_END);
    long length = ftell(file);
    char *byte_array = (char *)malloc((length+1)*sizeof(unsigned char));
    fseek(file, 0, SEEK_SET);
    fread(byte_array, sizeof(unsigned char), length, file);
    if(close_file) {
        fclose(file);
    }
    return byte_array;
}

FILE *open_file(char* filename, char* mode) {
    printf("Opening file %s...\n", filename);
    return fopen(filename, mode);
}

struct ForrestTreeNode * generate_empty_bytes_list() {
    struct ForrestTreeNode *ptr = create_forrest_tree();
    struct ForrestTreeNode *current = ptr;
    int i;
    for(i = 0; i <= 255; i++) {
        current->appearance_number = 0;
        current->frequency = 0;
        current->byte = (unsigned char) i;
        if(i < 255) {
            struct ForrestTreeNode *next = create_forrest_tree();
            current->next = next;
            next->previous = current;
            current = next;
        }
    }
    return ptr;
}

struct ForrestTreeNode *create_forrest_tree() {
    struct ForrestTreeNode *byteCode = (struct ForrestTreeNode *)malloc(sizeof(struct ForrestTreeNode));
    byteCode->next = NULL;
    byteCode->previous = NULL;
    byteCode->left = NULL;
    byteCode->right = NULL;
    byteCode->byte = (unsigned char)0;
    strcpy(byteCode->code,"");
    byteCode->frequency = 0;
    byteCode->appearance_number = 0;
    return byteCode;
}
