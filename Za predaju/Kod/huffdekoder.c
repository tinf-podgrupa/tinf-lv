//
// Created by MarinPetrunic on 3.1.2016..
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

struct ForrestTreeNode {
    unsigned char byte;
    long appearance_number;
    float frequency;
    char code[256];
    struct ForrestTreeNode *previous;
    struct ForrestTreeNode *next;
    struct ForrestTreeNode *left;
    struct ForrestTreeNode *right;
};

struct ForrestTreeNode *create_tree(FILE *pIobuf);

FILE *open_file(char *string, char string1[3]);

struct ForrestTreeNode *create_forrest_tree();

void add_leaves(struct ForrestTreeNode *pNode, char* bit, unsigned char i);

long get_file_size(FILE *pIobuf);

void fill_bit_array(unsigned char *array, FILE *pIobuf);

char* read_file_bytes(FILE* file, bool close_file);

void decode_file(unsigned char *array, struct ForrestTreeNode *pNode, FILE *pIobuf);

void write_byte_to_output(FILE *pIobuf, unsigned char byte);

int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf("Wrong number of arguments supplied!\n");
        exit(1);
    }
    FILE *table = open_file(argv[1], "r");
    FILE *input = open_file(argv[2], "rb");
    FILE *output = open_file(argv[3], "w");
    struct ForrestTreeNode *root = create_tree(table);
    long input_bytes_number = get_file_size(input);
    unsigned char* bit_array = (unsigned char*) malloc((size_t) ((input_bytes_number * 8+1) * sizeof(unsigned char)));
    fill_bit_array(bit_array, input);
    printf("First input bit is %c", *bit_array);
    decode_file(bit_array, root, output);
}

void decode_file(unsigned char *array, struct ForrestTreeNode *pNode, FILE *pIobuf) {
    unsigned char *bit;
    struct ForrestTreeNode* root_copy = pNode;
    for(bit = array; *bit != '\0'; bit++) {
        //printf("Current bit %c!\n", *bit);
        if(pNode->left != NULL && pNode->left->code[0] == *bit) {
            //printf("Went left!\n");
            pNode = pNode->left;
        } else if(pNode->right != NULL && pNode->right->code[0] == *bit) {
            //printf("Went right!\n");
            pNode = pNode->right;
        } else {
            //printf("Found byte %X!\n", pNode->byte);
            write_byte_to_output(pIobuf, pNode->byte);
            pNode = root_copy;
        }
    }
    fclose(pIobuf);
}

void write_byte_to_output(FILE *pIobuf, unsigned char byte) {
    fwrite(&byte, 1, sizeof(byte), pIobuf);
}


void fill_bit_array(unsigned char *array, FILE *pIobuf) {
    char *bytes = read_file_bytes(pIobuf, true);
    unsigned char *byte;
    for(byte = (unsigned char*)bytes; *byte != '\0'; byte++) {
        char mask = 1;
        int i;
        for (i = 0; i < 8; i++) {
            if((*byte & (mask << i)) != 0) {
                *array = '1';
            } else {
                *array = '0';
            }
            array++;
        }
    }
    *array = '\0';
}

char* read_file_bytes(FILE* file, bool close_file) {
    fseek(file, 0, SEEK_END);
    long length = ftell(file);
    char *byte_array = (char *)malloc((length+1)*sizeof(unsigned char));
    fseek(file, 0, SEEK_SET);
    fread(byte_array, sizeof(unsigned char), length, file);
    if(close_file) {
        fclose(file);
    }
    return byte_array;
}

long get_file_size(FILE *pIobuf) {
    fseek(pIobuf, 0L, SEEK_END);
    long bytes_number = ftell(pIobuf);
    fseek(pIobuf, 0L, SEEK_SET);
    return bytes_number;
}

struct ForrestTreeNode *create_tree(FILE *pIobuf) {
    char line[256];
    char c;
    int i = 0;
    int byte_count = 0;
    struct ForrestTreeNode *root = create_forrest_tree();
    while ((c = (char) fgetc(pIobuf)) != EOF) {
        if (c == '\n') {
            line[i] = '\0';
            add_leaves(root, line, (unsigned char) byte_count);
            i = 0;
            byte_count++;
        } else {
            line[i++] = c;
        }
    }
    return root;
}

void add_leaves(struct ForrestTreeNode *pNode, char* bit, unsigned char i) {
    while(*bit != '\0') {
        struct ForrestTreeNode* leaf = create_forrest_tree();
        if(*bit == '1') {
            strcpy(leaf->code, "1");
            if(pNode->left == NULL) {
                pNode->left = leaf;
            }
            if(*(++bit) == '\0') {
                pNode->left->byte = i;
            }
            bit--;
            pNode = pNode->left;
        } else {
            strcpy(leaf->code, "0");
            if(pNode->right == NULL) {
                pNode->right = leaf;
            }
            if(*(++bit) == '\0') {
                pNode->right->byte = i;
            }
            bit--;
            pNode = pNode->right;
        }
        bit++;
    }
}

FILE *open_file(char *string, char string1[3]) {
    printf("Opening file %s...\n", string);
    return fopen(string, string1);
}

struct ForrestTreeNode *create_forrest_tree() {
    struct ForrestTreeNode *byteCode = (struct ForrestTreeNode *) malloc(sizeof(struct ForrestTreeNode));
    byteCode->next = NULL;
    byteCode->previous = NULL;
    byteCode->left = NULL;
    byteCode->right = NULL;
    byteCode->byte = 0;
    strcpy(byteCode->code, "");
    byteCode->frequency = 0;
    byteCode->appearance_number = 0;
    return byteCode;
}