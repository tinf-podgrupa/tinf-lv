#pragma once

#ifndef DICT_H_   /* Include guard */
#define DICT_H_
#include "str.h"

typedef struct {
	String *key;
	int value;
} KeyValuePair;

typedef struct {
	KeyValuePair *data;
	int capacity;
	int count;
	int collisions;
} Dictionary;

Dictionary* new_Dictionary(int capacity);

int dict_add_or_get_value(Dictionary* dict, String *key, int value);

int dict_get_value(Dictionary* dict, String *key);

int _dict_debug_print_distribution(Dictionary *dict);

#endif // DICT_H_