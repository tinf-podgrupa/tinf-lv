
typedef struct {
	char *chars;
	int len;
} String;

int str_equals(String *s1, String *s2) {
	if (s1 == s2) return 1;
	if (s1->len != s2->len) return 0;
	for (int i = 0; i < s1->len; i++)
		if (s1->chars[i] != s2->chars[i]) return 0;
	return 1;
}