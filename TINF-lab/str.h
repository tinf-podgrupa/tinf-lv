#pragma once

#ifndef STR_H_   /* Include guard */
#define STR_H_

typedef struct {
	char *chars;
	int len;
} String;

int str_equals(String *s1, String *s2);

#endif // STR_H_
