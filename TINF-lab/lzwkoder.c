#include <stdio.h>
#include "str.h"
#include "dictionary.h"
#include "stringbuilder.h"

#define DICT_CAPACITY (int)(65536)
#define MAX_STR 2048

int main_lzwkoder(int argc, char **argv) {
	Dictionary *dict = new_Dictionary((int)(DICT_CAPACITY));
	unsigned short out[DICT_CAPACITY];
	int o = 0;

	FILE *fin;
	fopen_s(&fin, argv[1], "rb");

	StringBuilder* w = new_StringBuilder(MAX_STR);
	int n = 256;
	char c = fgetc(fin);
	sb_appendc(w, c);
	unsigned short code_prev = c;
	while (c = fgetc(fin), !feof(fin)) {
		sb_appendc(w, c);
		String *str = sb_to_string(w);
		int code = dict_add_or_get_value(dict, str, n);  // dodavanje rije�i (-1) ili pronala�enje postoje�e rije�i (kod)
		if (code != -1) {  // ve� postoji u rje�niku
			code_prev = code;
			continue;
		}
		out[o++] = code_prev;  // izlaz
		sb_clear(w);
		sb_appendc(w, c);
		code_prev = c;
		n++;  // novi kod za idu�u rije�
		if (n == DICT_CAPACITY) {
			puts("Nema vi�e mjesta u rje�niku.");
			return -1;
		}
	}
	out[o++] = code_prev;  // izlaz

	FILE* fout;
	fopen_s(&fout, argv[2], "wb");

	fwrite(out, sizeof(unsigned short), o, fout);

	return 0;
}