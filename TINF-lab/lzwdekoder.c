#include <stdio.h>
#include "str.h"
#include "stringbuilder.h"

#define DICT_CAPACITY (int)(65536)
#define MAX_STR 2048

int main_lzwdekoder(int argc, char **argv) {
	String **dict = malloc(DICT_CAPACITY*sizeof(String*));
	dict -= 256;

	FILE *fin;
	fopen_s(&fin, argv[1], "rb");
	FILE* fout;
	fopen_s(&fout, argv[2], "wb");

	unsigned short code;
	fread_s(&code, sizeof(unsigned short), sizeof(unsigned short), 1, fin);
	StringBuilder *w = new_StringBuilder(MAX_STR);
	fputc((char)code, fout);
	sb_appendc(w, (char)code);
	int n = 256;
	while (1 == fread_s(&code, sizeof(unsigned short), sizeof(unsigned short), 1, fin)) {
		if (code != n) {  // rije� ve� postoji u rje�niku
			sb_appendc(w, code >= 256 ? dict[code]->chars[0] : (char)code);
			dict[n] = sb_to_string(w);
			sb_clear(w);
			if (code >= 256) {
				fwrite(dict[code]->chars, sizeof(char), dict[code]->len, fout);
				sb_appendstr(w, dict[code]);
			}
			else
			{
				fputc((char)code, fout);
				printf("%c\n", (char)code);
				sb_appendc(w, (char)code);
			}
		}
		else {  // n = code, nova rije�
			sb_appendc(w, w->chars[0]);
			dict[n] = sb_to_string(w);
			fwrite(dict[n]->chars, sizeof(char), dict[n]->len, fout);
		}
		n++;
	}
	return 0;
}