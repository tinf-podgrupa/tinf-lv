#include <stdio.h>

typedef unsigned char uint8;
typedef unsigned short uint16;

const uint8 A[8] = {
	0xF0,	// 11110000
	0xCC,	// 11001100
	0xAA,	// 10101010
	0x69,	// 01101001
	0x53,	// 01010011
	0x95,	// 10010101
	0x1E,	// 00011110
	0x27,	// 00100111
};

uint8 syndromes[8 + 120];
uint16 e_vectors[8 + 120];

uint16 get_syndrome(uint16 lbbcode) {
	const uint16 mask = 0x8000;
	uint16 synd = 0;
	for (int i = 0; i < 8; i++)
		if (lbbcode & (mask >> i))
			synd ^= A[i];
	synd ^= lbbcode & 0x00FF;
	return synd;
}

uint8 decode(uint16 lbbcode) {
	uint16 synd = get_syndrome(lbbcode);
	for (int i = 0; i < 16 + 120; i++)
		if (synd == syndromes[i])
			return (lbbcode ^ e_vectors[i]) >> 8;
	return 0;
}

void create_table() {
	int k = 16;
	for (k = 0; k < 16; k++) {
		e_vectors[k] = 1 << k;
		syndromes[k] = get_syndrome(1 << k);
	}
	for (int i = 1; i < 16; i++)
		for (int j = 0; j < i; j++, k++)
		{
			e_vectors[k] = e_vectors[i] | e_vectors[j];
			syndromes[k] = syndromes[i] ^ syndromes[j];
		}

	for (int j = 0; j < 8 + 120; j++)
	{
		printf("\n");
		for (int i = 0; i < 8; i++)
			printf("%c", (syndromes[j] & (0x80 >> i)) ? '1' : '0');
	}
}

int main_linbindekoder(int argc, char **argv) {	
	FILE *fin;
	fopen_s(&fin, argv[1], "rb");
	FILE* fout;
	fopen_s(&fout, argv[2], "wb");

	uint16 c;
	uint8 b;
	while (fread_s(&c, sizeof(uint16), sizeof(uint16), 1, fin), !feof(fin)) {
		b = decode(c);
		fputc(b, fout);
	}

	return 0;
}