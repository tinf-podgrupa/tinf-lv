#include <string.h>
#include <malloc.h>
#include <stdio.h>
#include "str.h"

typedef struct {
	String *key;
	int value;
} KeyValuePair;

unsigned long _get_hash(String *niz) {
	unsigned long h = 65599;
	for (int i = 0; i < niz->len; i++) h = 2053 * h + niz->chars[i]; // 997 0.074, 2053 0.072699
	return h;
}

typedef struct {
	KeyValuePair *data;
	int capacity;
	int count;
	int collisions;
} Dictionary;

void _dict_init(Dictionary *dict, int capacity) {
	dict->data = calloc(capacity, sizeof(KeyValuePair));
	dict->capacity = capacity;
	dict->count = 0;
	dict->collisions = 0;
}

Dictionary* new_Dictionary(int min_capacity) {
	Dictionary *d = malloc(sizeof(Dictionary));
	_dict_init(d, min_capacity*1.3);
	return d;
}

int dict_add_or_get_value(Dictionary *dict, String *key, int value) {  // vra�a KeyValuePair.value ako postoji, ina�e -1
	int addr = _get_hash(key) % dict->capacity;
	int a = addr;
	while (dict->data[a].key != NULL) {
		if (str_equals(key, dict->data[a].key)) return dict->data[a].value;
		if (++a == dict->capacity) a = 0;
	}
	dict->data[a] = (KeyValuePair) { key, value };
	dict->count++;
	dict->collisions += (a >= addr) ? a - addr : a - addr + dict->capacity;
	return -1;
}

int dict_get_value(Dictionary *dict, String *key) {  // vra�a KeyValuePair.value ako postoji, ina�e -1
	int a = _get_hash(key) % dict->capacity;
	while (dict->data[a].key != NULL) {
		if (str_equals(key, dict->data[a].key)) return dict->data[a].value;
		if (++a == dict->capacity) a = 0;
	}
	return -1;
}

void _dict_debug_print_distribution(Dictionary *dict) {
	for (int i = 0; i < dict->capacity; i++) {
		if (dict->data[i].key == NULL) putc(' ', stdout);
		else putc(220, stdout);
	}
	printf("\ncollisions: %f", ((float)dict->collisions*dict->count) / dict->capacity / dict->capacity);
}