#pragma once

#ifndef SB_H_   /* Include guard */
#define SB_H_

#include "str.h"

typedef struct {
	char* chars;
	int len;
} StringBuilder;

StringBuilder* new_StringBuilder(int capacity);

void sb_appendc(StringBuilder* sb, char c);

void sb_appendstr(StringBuilder* sb, String *s);

void sb_clear(StringBuilder* sb);

String *sb_to_string(StringBuilder* sb);

#endif // SB_H_