#include <string.h>
#include <malloc.h>
#include "str.h"

typedef struct {
	char* chars;
	int len;
} StringBuilder;

StringBuilder* new_StringBuilder(int capacity) {
	StringBuilder* str = malloc(sizeof(StringBuilder));
	str->chars = malloc(capacity*sizeof(char));
	str->chars[0] = '\0';
	str->len = 0;
	return str;
}

void sb_appendc(StringBuilder* sb, char c) {
	sb->chars[sb->len++] = c;
	sb->chars[sb->len] = '\0';
}

void sb_appendstr(StringBuilder* sb, String *s) {
	for (int i = 0; i < s->len; i++) sb->chars[i] = s->chars[i];
	sb->len = s->len;
}

void sb_clear(StringBuilder* sb) {
	sb->chars[0] = '\0';
	sb->len = 0;
}

String *sb_to_string(StringBuilder* sb) {
	char *s = malloc(sb->len*sizeof(char));
	for (int i = 0; i <= sb->len; i++) s[i] = sb->chars[i];
	String *str = malloc(sizeof(String));
	*str = (String) { s, sb->len };
	return str;
}