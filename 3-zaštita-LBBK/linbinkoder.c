#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>

typedef unsigned char uint8;
typedef unsigned short uint16;

const uint8 A[8] = {
	0xF0,	// 11110000
	0xCC,	// 11001100
	0xAA,	// 10101010
	0x69,	// 01101001
	0x53,	// 01010011
	0x95,	// 10010101
	0x1E,	// 00011110
	0x27,	// 00100111
};

uint16 encode(uint8 code) {
	const uint8 mask = 1 << 7;
	uint16 lbbcode = code << 8;
	for (int i = 0; i < 8; i++)
		if (code & (mask >> i))
			lbbcode ^= A[i];
	return lbbcode;
}

int main(int argc, char **argv) {
	FILE *fin = fopen(argv[1], "rb");
	FILE* fout = fopen(argv[2], "wb");

	uint8 b;
	uint16 c;
	while (b = fgetc(fin), !feof(fin)) {
		c = encode(b);
		fwrite(&c, sizeof(uint16), 1, fout);
	}

	return 0;
}