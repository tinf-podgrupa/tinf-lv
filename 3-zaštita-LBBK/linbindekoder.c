#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>

typedef unsigned char uint8;
typedef unsigned short uint16;

const uint8 A[8] = {
	0xF0,	// 11110000
	0xCC,	// 11001100
	0xAA,	// 10101010
	0x69,	// 01101001
	0x53,	// 01010011
	0x95,	// 10010101
	0x1E,	// 00011110
	0x27,	// 00100111
};

uint8 syndromes[16 + 120];
uint16 e_vectors[16 + 120];

uint8 get_syndrome(uint16 lbbcode) {
	const uint16 mask = 1 << 15;
	uint8 synd = (uint8)lbbcode;  // zadnjih 8 bitova (redindantni bitovi)
	for (int i = 0; i < 8; i++)
		if (lbbcode & (mask >> i))
			synd ^= A[i];
	return synd;
}

uint8 decode(uint16 lbbcode) {
	uint8 synd = get_syndrome(lbbcode);
	if (synd == 0) return lbbcode >> 8;
	for (int i = 0; i < 16 + 120; i++)
		if (synd == syndromes[i])
			return (lbbcode ^ e_vectors[i]) >> 8;
	return lbbcode >> 8;
}

void create_table() {
	const uint16 mask = 1 << 15;
	int k;
	for (k = 0; k < 16; k++) {
		e_vectors[k] = mask >> k;
		syndromes[k] = get_syndrome(e_vectors[k]);
	}
	for (int i = 1; i < 16; i++)
		for (int j = 0; j < i; j++) {
			e_vectors[k] = e_vectors[i] | e_vectors[j];
			syndromes[k] = syndromes[i] ^ syndromes[j];
			k++;
		}
}

int main(int argc, char **argv) {
	FILE *fin = fopen(argv[1], "rb");
	FILE* fout = fopen(argv[2], "wb");

	create_table();
	uint16 c;
	uint8 b;
	while (fread(&c, sizeof(uint16), 1, fin), !feof(fin)) {
		b = decode(c);
		fputc(b, fout);
	}

	return 0;
}